﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BabbysFirstWebFramework;

namespace Driver
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new BabbysFirstWebServer();
            server.Start();
        }
    }
}
