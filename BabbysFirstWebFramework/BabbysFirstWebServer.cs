﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using WebFramework;

namespace BabbysFirstWebFramework
{
    public class BabbysFirstWebServer : Server
    {
        public BabbysFirstWebServer()
            : base()
        {
        }

        public override async Task<IHtmlElement> HandleRequest(HttpListenerContext context)
        {
            var request = context.Request;
            var response = context.Response;

            var args = request.RawUrl.Trim('/').Split('/');

            return new BabbysFirstWebpage(args);
        }
    }
}
