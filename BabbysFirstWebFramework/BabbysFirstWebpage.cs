﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WebFramework;

using Person = System.Tuple<string, int>;

namespace BabbysFirstWebFramework
{
    public class BabbysFirstWebpage : IHtmlElement
    {
        private readonly HtmlPage _Page;

        public BabbysFirstWebpage(string[] args)
        {
            var title = args.Length > 0 && !string.IsNullOrEmpty(args[0])
                ? args[0]
                : "Babby's First Web Framework";

            var body = args.Length > 1 && !string.IsNullOrEmpty(args[1])
                ? args[1]
                : "Welcome to my webpage!";

            var people = new Person[]
            {
                new Person("Curtis", 25),
                new Person("Tony", 25),
                new Person("Rhys", 20)
            };

            Func<Person, HtmlTableRow<Person>> personToRow = person => new HtmlTableRow<Person>(new string[] {person.Item1, person.Item2.ToString()});

            _Page = new HtmlPage(
                new HtmlHead(title),
                new HtmlBody(
                    new HtmlParagraph(body),
                    new HtmlTable<Person>(people, personToRow)));
        }

        public string Serialize()
        {
            return _Page.Serialize();
        }
    }
}
