﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlTitle : HtmlPrimitive
    {
        public HtmlTitle(string title)
            : base("title", title)
        {
        }
    }
}
