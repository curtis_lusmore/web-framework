﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlPage : HtmlContainer
    {
        public HtmlPage(HtmlHead head, HtmlBody body)
            : base("html", head, body)
        {
        }
    }
}
