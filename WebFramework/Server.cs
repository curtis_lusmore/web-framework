﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public abstract class Server
    {
        private readonly HttpListener _HttpListener;

        public Server()
        {
            _HttpListener = new HttpListener();
            _HttpListener.Prefixes.Add("http://localhost:80/");
        }

        public void Start()
        {
            _HttpListener.Start();
            Run().Wait();            
        }

        public async Task Run()
        {
            while (true)
            {
                var context = await _HttpListener.GetContextAsync();
                var page = await HandleRequest(context);

                var stream = new StreamWriter(context.Response.OutputStream);
                stream.Write(page.Serialize());
                stream.Close();
            }
        }

        public abstract Task<IHtmlElement> HandleRequest(HttpListenerContext context);

        public async Task<HttpListenerContext> Listen()
        {
            return await _HttpListener.GetContextAsync();
        }
    }
}
