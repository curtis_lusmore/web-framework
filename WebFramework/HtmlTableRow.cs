﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlTableRow<TSource> : HtmlContainer
    {
        public HtmlTableRow(IEnumerable<HtmlTableCell> cells)
            : base("tr", cells.ToArray())
        {
        }

        public HtmlTableRow(IEnumerable<string> cells)
            : this(cells.Select(cell => new HtmlTableCell(cell)))
        {
        }

        public HtmlTableRow(TSource source, Func<TSource, IEnumerable<HtmlTableCell>> selector)
            : this(selector(source))
        {
        }

        public HtmlTableRow(TSource source, Func<TSource, IEnumerable<string>> selector)
            : this(selector(source))
        {
        }
    }
}
