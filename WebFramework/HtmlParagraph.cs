﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlParagraph : HtmlPrimitive
    {
        public HtmlParagraph(string contents)
            : base("p", contents)
        {
        }
    }
}
