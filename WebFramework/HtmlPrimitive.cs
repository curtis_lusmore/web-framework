﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public abstract class HtmlPrimitive : IHtmlElement
    {
        private readonly string _Tag;
        private readonly string _Contents;

        public HtmlPrimitive(string tag, string contents)
        {
            _Tag = tag;
            _Contents = contents;
        }

        public string Serialize()
        {
            return string.Format(
                "<{0}>{1}</{0}>",
                _Tag,
                _Contents);
        }
    }
}
