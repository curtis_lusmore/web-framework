﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlBody : HtmlContainer
    {
        public HtmlBody(params IHtmlElement[] contents)
            : base("body", contents)
        {
        }
    }
}
