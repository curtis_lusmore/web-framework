﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlTableCell : HtmlPrimitive
    {
        public HtmlTableCell(string contents)
            : base("td", contents)
        {
        }
    }
}
