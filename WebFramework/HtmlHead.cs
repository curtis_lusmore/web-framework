﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlHead : HtmlContainer
    {
        public HtmlHead(string title)
            : base("head", new HtmlTitle(title))
        {
        }
    }
}
