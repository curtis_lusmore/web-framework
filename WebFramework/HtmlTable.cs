﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public class HtmlTable<TSource> : HtmlContainer
    {
        public HtmlTable(IEnumerable<HtmlTableRow<TSource>> rows)
            : base("table", rows.ToArray())
        {
        }

        public HtmlTable(IEnumerable<TSource> rows, Func<TSource, HtmlTableRow<TSource>> selector)
            : this(rows.Select(selector))
        {
        }

        public HtmlTable(IEnumerable<TSource> rows, Func<TSource, IEnumerable<string>> selector)
            : this(rows.Select(selector).Select(row => new HtmlTableRow<TSource>(row)))
        {
        }
    }
}
