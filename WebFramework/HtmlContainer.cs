﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebFramework
{
    public abstract class HtmlContainer : IHtmlElement
    {
        private readonly string _Tag;
        private readonly IHtmlElement[] _Contents;

        public HtmlContainer(string tag, params IHtmlElement[] contents)
        {
            _Tag = tag;
            _Contents = contents;
        }

        public string Serialize()
        {
            return string.Format(
                "<{0}>{1}</{0}>",
                _Tag,
                _Contents.Aggregate(string.Empty, (prev, next) => prev + next.Serialize()));
        }
    }
}
